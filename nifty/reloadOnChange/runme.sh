#!/bin/sh

find ../../src/ \( -name '*.js' -or -name '*.html' -or -name '*.css' \) -print | entr ./reload-browser firefox
