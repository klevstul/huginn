// 18.12.16 // frode burdal klevstul // www.thisworld.is

import {HuginnCfg}              from './classes/huginncfg.class.KDBUILD.js';
import {WwwService}             from './services/www.service.KDBUILD.js'
import {CfgLoaderService}       from './services/cfgloader.service.KDBUILD.js';
import {LayoutService}          from './services/layout.service.KDBUILD.js';
import {ExtractorService}       from './services/extractor.service.KDBUILD.js';

let huginn_cfg: HuginnCfg = new HuginnCfg;
let wwwService: WwwService = new WwwService;
let cfgLoader: CfgLoaderService = new CfgLoaderService;
let layoutService: LayoutService = new LayoutService;
let extractorService: ExtractorService = new ExtractorService;
let request_parameters: string = wwwService.getRequestParameters();
let service_id = extractorService.getParameterValueFromRequestUrl(request_parameters, 'service_id');

if (service_id == ""){
    throw new Error('service_id not found in request parameters')
}

// session storage config
let sscfg = sessionStorage.getItem("huginn_cfg." + service_id);

if (!sscfg){

    // request config from muninn
    cfgLoader.loadHuginnConfig(request_parameters, function(callback_huginn_cfg: HuginnCfg){
        huginn_cfg = callback_huginn_cfg;
        sessionStorage.setItem("huginn_cfg." + service_id, JSON.stringify(callback_huginn_cfg));
        layoutService.setUpPage(huginn_cfg, request_parameters);
    });

} else {

    // load config from browser's session storage
    huginn_cfg = JSON.parse(sscfg);
    layoutService.setUpPage(huginn_cfg, request_parameters);

}
