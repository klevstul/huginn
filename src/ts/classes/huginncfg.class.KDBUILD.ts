// 18.12.28 // frode burdal klevstul // www.thisworld.is

export class HuginnCfg {

    analytics:
    {
        enabled:            string;
        javascript:         string;
        noscript_image:     string;
    }

    rpc:
    {
        cache:              string;
        index:              string;
        service_id:         string;
    }

    commenting:
    {

        div_attributes:     Array<string>;
        div_class:          Array<string>;
        div_id:             string;
        enabled:            string;
        javascript:         string;
        request_exclude:    Array<string>;
        request_include:    Array<string>;
        script_attributes:  Array<string>;
    }

    layout:
    {
        css_content:        string;
        css_file_to_load:   string;
        css_link:           string;
        footer:             string;
        header:             string;
        title:              string;
    }

    meta:
    {
        author:             string;
        description:        string;
        keywords:           string;
    }

    miscellaneous:
    {
        cdn:                string;
    }

    muninn_version:
    {
        name:               string;
        number:             string;
    }

}
